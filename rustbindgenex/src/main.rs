#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[allow(dead_code)]
const SDL_INIT_EVERYTHING: Uint32 = 62001;
const SDL_INIT_VIDEO: Uint32 = 32;
const SDL_WINDOW_SHOWN: Uint32 = 4;
const SDL_KEYDOWN: Uint32 = 768;
const SDLK_Q: Uint32 = 113;
const SDL_QUIT: Uint32 = 256;

fn main() {
    let mut window = SDL_Window { _unused: [] };
    let mut renderer = SDL_Renderer { _unused: [] };

    unsafe {
        let x = SDL_Init(SDL_INIT_VIDEO);
        if x != 0 {
            println!("Could not init SDL: {}", x);
            return;
        }

        let mut mut_ptr_window = std::ptr::addr_of_mut!(window);
        let mut mut_ptr_renderer = std::ptr::addr_of_mut!(renderer);
        let x = SDL_CreateWindowAndRenderer(
            800,
            600,
            SDL_WINDOW_SHOWN,
            std::ptr::addr_of_mut!(mut_ptr_window),
            std::ptr::addr_of_mut!(mut_ptr_renderer),
        );
        if x != 0 {
            println!("Could not create window");
            return;
        }

        let mut running = true;

        while running {
            let rect = SDL_Rect {
                x: 200,
                y: 200,
                w: 120,
                h: 120,
            };

            let x = SDL_SetRenderDrawColor(mut_ptr_renderer, 51, 51, 151, 255);
            if x != 0 {
                println!("Error on set color");
                break;
            }

            SDL_RenderFillRect(mut_ptr_renderer, &rect);

            SDL_RenderPresent(mut_ptr_renderer);

            let mut event = SDL_Event { type_: 0 };
            let mut_ptr_event = std::ptr::addr_of_mut!(event);
            while SDL_PollEvent(mut_ptr_event) != 0 {
                match event.type_ {
                    SDL_KEYDOWN => {
                        let result = event.key.keysym.sym;
                        if result == SDLK_Q as i32 {
                            running = false;
                            break;
                        }
                    }
                    SDL_QUIT => {
                        running = false;
                        break;
                    }
                    _ => (),
                }
            }
        }

        SDL_DestroyRenderer(mut_ptr_renderer);
        SDL_DestroyWindow(mut_ptr_window);
        SDL_Quit();
    }

    
    println!("Hello, world!");
}
