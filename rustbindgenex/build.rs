extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    // println!("cargo:rustc-link-search=/usr/include/SDL2/");
    println!("cargo:rustc-link-lib=SDL2");
    println!("cargo:rerun-if-changed=wrapper.h");
    
    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .allowlist_type("SDL_Window")
        .allowlist_type("SDL_Renderer")
        .allowlist_type("SDL_Event")
        .allowlist_type("SDL_Rect")
        .allowlist_function("SDL_Init")
        .allowlist_function("SDL_CreateWindowAndRenderer")
        .allowlist_function("SDL_SetRenderDrawColor")
        .allowlist_function("SDL_RenderFillRect")
        .allowlist_function("SDL_RenderPresent")
        .allowlist_function("SDL_PollEvent")
        .allowlist_function("SDL_DestroyRenderer")
        .allowlist_function("SDL_DestroyWindow")
        .allowlist_function("SDL_Quit")
        .clang_arg("-I/usr/include/SDL2")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldnt write bindings");
}
