#include <SDL2/SDL.h>
#include <stdio.h>

#define WIDTH 800
#define HEIGHT 600
#define QUITKEY SDLK_q

SDL_Window* screen = NULL;
SDL_Renderer* renderer;
SDL_Event event;

int main(void) {
  if (SDL_Init(SDL_INIT_EVERYTHING)) {
    printf("Couldn't init\n");
    return 1;
  }

  SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, SDL_WINDOW_SHOWN,
                              &screen, &renderer);
  if (!screen) {
    printf("Couldnt create window\n");
    return 1;
  }

  int running = 1;
  while (running) {
    SDL_Rect rect;
    SDL_SetRenderDrawColor(renderer, 51, 51, 151, 255);
    rect.h = 120;
    rect.w = 120;
    rect.x = 200;
    rect.y = 200;
    SDL_RenderFillRect(renderer, &rect);


    SDL_RenderPresent(renderer);

    int result = -1;
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
      case SDL_KEYDOWN:
        result = event.key.keysym.sym;
        if (result == QUITKEY) {
          running = 0;
          break;
        }
        break;
      case SDL_QUIT:
        running = 0;
        break;
      }
    }
  }


  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(screen);
  SDL_Quit();
  
  return 0;
}
